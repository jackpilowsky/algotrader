const StockModel = require('../models/StockModel');
const stockModel = new StockModel();

class StockController{
  list(query){
    return stockModel.list(query);
  }
}
module.exports = StockController;