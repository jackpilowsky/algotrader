const express = require('express');
const router = express.Router();
const StockController = require('../controllers/StockController');
const stocktController = new StockController();

router.get("/api/stocks", (req, res) => {
  const query =  req.query.q || '';
  res.json(stocktController.list(query));
})

module.exports = router;