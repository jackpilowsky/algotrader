const express = require('express');
const router = express.Router();
const AccountController = require('../controllers/AccountController');
const accountController = new AccountController();

router.post("/api/login", (req, res) => {
  let session = req.session;
  const username = req.body.username || false;
  const password = req.body.password || false;
  res.json(accountController.login(session, {
    username: username,
    password: password
  }));
})


router.post("/api/register", (req, res) => {
  let session = req.session;
  const username = req.body.username || false;
  const password = req.body.password || false;
  res.json(accountController.register(session, {
    username: username,
    password: password
  }));
})

module.exports = router;