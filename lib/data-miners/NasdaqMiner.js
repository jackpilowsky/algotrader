const request = require('request');
const cheerio = require('cheerio');
const StockModel = require('../models/StockModel');
const stockModel = new StockModel();

class NasdaqMiner {
  constructor(opts){
    opts = opts || {};
    this.url = opts.url || 'http://www.nasdaq.com/screening/company-list.aspx';
    this.selector = opts.selector || '#companyListDownloads tr td:nth-child(1) a';
    this.subselector = opts.subselector || '#resultsDisplay a';
  }
  // this method is starting to develop an arrow anti-pattern. Might need to split it up if it gets worse
  mine(){
    let self = this;
    // prepare for mining
    stockModel.createTable();
    stockModel.emptyTable();
    // make original request
    self.sendRequest(self.url, 1, (html) => {
      let $ = cheerio.load(html);
      $(self.selector).each((i, el) => {
        // make secondary request
        let href = $(el).attr('href');
        self.sendRequest(href, 2, (html) => {
          let $ = cheerio.load(html);
          let csvHref = 'http://www.nasdaq.com/screening/' + $(self.subselector).attr('href');
          // make csv request
          self.sendRequest(csvHref, 3, (html, response) => {
            const rows = response.body.split("\n");
            const stocks = rows.map((row) => {
              let columns = row.split(',').map((column) =>{
                return column.replace(/["]/g, ''); // remove double quotes from CSV
              });
              let stock = {};

              if(columns && columns.length > 2 && columns[0].toLowerCase() !== 'symbol' && columns[1].toLowerCase() !== 'name'){
                stock.symbol = columns[0];
                stock.companyname = columns[1];
                stockModel.create(stock);
              }
              return stock;
            });
          });
        })
      })
    })
  } 
  sendRequest(url, level, cb){
    request(url, (error, response, html) => {
      if(!error){
        if(cb){
          cb(html, response);
        }
      }else{
        console.log('Scrapper error level '+ level +': ' + error);
      }
    });  
  }  
}

module.exports = NasdaqMiner;