const path = require('path');
const fs = require("fs");
const sqlite = require("sql.js");
const filebuffer = fs.readFileSync( path.dirname(require.main.filename) + "/db/usda-nnd.sqlite3");
const db = new sqlite.Database(filebuffer);

var appDir = path.dirname(require.main.filename);

class Model{
  constructor(){
    if(this.createTable){
      this.createTable();
    } 
  }
  _runQuery(queryStr){
    return this._queryToArray(db.exec(queryStr));
  }
  _runQueryWithCommit(queryStr){
    db.exec("BEGIN TRANSACTION;" + queryStr + "COMMIT;" );
    return;
  }
  _queryToArray(query){
    const array =[];
    if(query[0]){
      query[0].values.map(function(value, index){
        array.push({});
        query[0].columns.map(function(column, columnIndex){
          array[index][column] = value[columnIndex];
        });
      });
    }
    return array;
  }
}
module.exports = Model;