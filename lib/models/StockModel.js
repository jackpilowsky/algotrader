const Model = require('./Model');

class StockModel extends Model{  
  createTable(){
    const query = `
      CREATE TABLE IF NOT EXISTS stocks (
        stockid integer primary key autoincrement, 
        symbol varchar(50),
        companyname varchar(100)
      );
    `
    this._runQueryWithCommit(query);
  }
  emptyTable(){
    const query = `
          DELETE FROM stocks;
        `
        this._runQueryWithCommit(query);
  }
  deleteTable(){
    const query = `
      DROP TABLE IF EXISTS stocks;
    `
    this._runQueryWithCommit(query);
  }
  create(opts){
    const query = `
      INSERT INTO stocks (symbol, companyname)
      VALUES('${opts.symbol}', '${opts.companyname}');
      SELECT last_insert_rowid();
    `
    this._runQueryWithCommit(query);
  }
  get(opts){
    if(!opts.symbol && !opts.stockid){
      console.log('Error, symbol nor stockid not provided for get. Opts: ' + opts);
    }
    let query = 'SELECT stockid, symbol, companyname FROM stocks WHERE 1 = 1 ';
    if(opts.symbol){
      query += 'AND symbol = ${opts.symbol} '
    }
    if(opts.stockid){
      query += 'AND stockid = ${opts.stockid} '
    }
    return this._runQuery(query);
  }
  list(searchText){
    console.log(searchText)
    let query = `SELECT stockid, symbol, companyname FROM stocks `;
    if(searchText){
      query += `WHERE symbol LIKE '%${searchText}%' OR companyname LIKE '%${searchText}%'`
    }
    console.log(query)
    return this._runQuery(query);
  }
  update(opts){
    if(!opts.stockid || (!opts.symbol && !opts.companyname) ){
      console.log('Error, stock should not be updated. Opts: ' + opts);
    }
    let query = 'UPDATE stocks SET ';
    if(opts.symbol){
      query += 'symbol = ${opts.symbol}'
    }
    if(opts.companyname){
      query += 'companyname = ${opts.companyname}'
    }
    if(opts.stockid){
      query += 'WHERE stockid = ${opts.stockid}'
    }
    this._runQueryWithCommit(query);
  }
  delete(opts){
    if(!opts.stockid){
      console.log('Error, stock should not be deleted. Opts: ' + opts);
    }
    let query = 'DELETE FROM stocks WHERE stockid = ${opts.stockid}';
    this._runQueryWithCommit(query);
  }
}

module.exports = StockModel;