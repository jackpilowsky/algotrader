const Model = require('./Model');

class AccountModel extends Model{  
  create(opts){
    const query = `
      INSERT INTO accounts (username, password)
      VALUES(${opts.username}, ${opts.password});
      SELECT last_insert_rowid();
    `
    this._runQueryWithCommit(query);
  }
  update(opts){
    if(!opts.accountid){
      console.log('Error, user should not be updated. Opts: ' + opts);
    }
    let query = 'UPDATE accounts SET ';
    if(opts.username){
      query += 'username = ${opts.username} '
    }
    if(opts.password){
      query += 'password = ${opts.username} '
    }
    if(opts.accountid){
      query += 'WHERE accountid = ${opts.accountid}' 
    }
    this._runQueryWithCommit(query);
  }
  get(opts){
    if(!opts.username && !opts.accountid){
      console.log('Error, username and id not provided for get. Opts: ' + opts);
    }
    let query = 'SELECT username, accountid, password FROM accounts WHERE 1 = 1 ';
    if(opts.username){
      query += 'AND username = ${opts.username}'
    }
    if(opts.accountid){
      query += 'AND accountid = ${opts.accountid}'
    }
    return this._runQuery(query);
  }
  list(){
    const query = `
      SELECT username, accountid, password FROM accounts
    `
    return this._runQuery(query);
  }
  delete(opts){
    if(!opts.username && !opts.accountid){
      console.log('Error, user should not be deleted. Opts: ' + opts);
    }
    let query = 'DELETE FROM accounts WHERE 1 = 1 ';
    if(opts.username){
      query += 'AND username = ${opts.username}'
    }
    if(opts.accountid){
      query += 'AND accountid = ${opts.accountid}'
    }
  }
  createTable(){
    const query = `
          CREATE TABLE IF NOT EXISTS accounts (
            accountid integer primary key autoincrement, 
            username varchar(50),
            password varchar(100)
          );
        `
        this._runQueryWithCommit(query);
  }
  emptyTable(){
    const query = `
          DELETE FROM accounts;
        `
        this._runQueryWithCommit(query);
  }
  deleteTable(){
    const query = `
          DROP TABLE IF EXISTS accounts;
        `
    this._runQueryWithCommit(query);
  }
}
module.exports = AccountModel;