import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown, MenuItem  } from 'react-bootstrap';

class MainMenu extends Component{
  render(){
    const hash = window.location.hash;
    return (
      <Navbar.Collapse>
        <Nav activeHref={hash}>
          <NavDropdown eventKey={1} title="Markets" id="main-menu-markets">
            <MenuItem eventKey={1.1} href="#">Markets</MenuItem>
            <MenuItem eventKey={1.2} href="#premarket">Pre-Market</MenuItem>
            <MenuItem eventKey={1.3} href="#index/market-movers">Market Movers</MenuItem>
            <MenuItem eventKey={1.4} href="#index/dow-jones">Dow Jones Live</MenuItem>
            <MenuItem eventKey={1.5} href="#currencies/realtime-list">Currencies Live</MenuItem>
            <MenuItem eventKey={1.6} href="#commodities/realtime-list">Commodities Live</MenuItem>
            <MenuItem eventKey={1.7} href="#bankrate">Rates</MenuItem>
            <MenuItem eventKey={1.8} href="#mymarkets">Your Portfolio</MenuItem>
          </NavDropdown>
          <NavDropdown eventKey={2} title="Stocks" id="main-menu-stocks">
            <MenuItem eventKey={2.1} href="#stocks">Stocks</MenuItem>
            <MenuItem eventKey={2.2} href="#stocks/find-stocks">Stocks Finder</MenuItem>
            <MenuItem eventKey={2.3} href="#index/market-movers">Market Movers</MenuItem>
            <MenuItem eventKey={2.4} href="#index/components">Index Constituents</MenuItem>
            <MenuItem eventKey={2.5} href="#commodities/realtime-list">Commodities</MenuItem>
            <MenuItem eventKey={2.6} href="#currencies/realtime-list">Currencies</MenuItem>
            <MenuItem eventKey={2.7} href="#stocks/dividends">Dividends</MenuItem>
          </NavDropdown>
          <NavDropdown eventKey={3} title="Market Movers" id="main-menu-market-movers">
            <MenuItem eventKey={3.1} href="#market-movers">Market Movers</MenuItem>
            <MenuItem eventKey={3.2} href="#index/market-movers">Market Movers</MenuItem>
            <MenuItem eventKey={3.3} href="#index/components">Index Constituents</MenuItem>
            <MenuItem eventKey={3.4} href="#index/dow_jones">Dow Jones Live</MenuItem>
            <MenuItem eventKey={3.5} href="#index/S\u0026P_500">S&amp;P 500 Live</MenuItem>
            <MenuItem eventKey={3.6} href="#index/NASDAQ_100">NASDAQ Live</MenuItem>
          </NavDropdown>
          <NavDropdown eventKey={4} title="Commodities" id="main-menu-commodities">
            <MenuItem eventKey={4.1} href="#commodities">Commodities</MenuItem>
            <MenuItem eventKey={4.1} href="#commodities/realtime-list">Commodities Live</MenuItem>
            <MenuItem eventKey={4.1} href="#commodities/gold-price">Gold Price</MenuItem>
            <MenuItem eventKey={4.1} href="#commodities/oil-price?type=WTI">Oil</MenuItem>
            <MenuItem eventKey={4.1} href="#commodities/copper-price">Copper</MenuItem>
          </NavDropdown>
          <NavDropdown eventKey={5} title="Currencies" id="main-menu-currencies">
            <MenuItem eventKey={5.1} href="#Currencies">Currencies</MenuItem>
            <MenuItem eventKey={5.2} href="#news/ressort/currencies">News</MenuItem>
            <MenuItem eventKey={5.3} href="#currencies/realtime-list">Currencies Live</MenuItem>
            <MenuItem eventKey={5.4} href="#urrency-converter">Currency Converter</MenuItem>
            <MenuItem eventKey={5.5} href="#currencies/EUR-USD">EUR/USD</MenuItem>
            <MenuItem eventKey={5.6} href="#currencies/GBP-USD">GBP/USD</MenuItem>
            <MenuItem eventKey={5.7} href="#currencies/BTC-USD">Bitcoin</MenuItem>
            <MenuItem eventKey={5.8} href="#index/us-dollar-index">US Dollar Index</MenuItem>
          </NavDropdown>
          <NavDropdown eventKey={6} title="Mutual Funds" id="main-menu-mutual-funds" >
            <MenuItem eventKey={6.1} href="#mutual-funds">Mutual Funds</MenuItem>
            <MenuItem eventKey={6.2} href="#news/ressort/funds">News</MenuItem>
            <MenuItem eventKey={6.3} href="#funds/mutual-funds/finder">Mutual Funds Finder</MenuItem>
            <MenuItem eventKey={6.4} href="#funds/investmentcompanies">Mutual Funds Issuers</MenuItem>
          </NavDropdown>
          <NavDropdown eventKey={7} title="ETFs" id="main-menu-etfs">
            <MenuItem eventKey={7.1} href="#etfs">ETFs</MenuItem>
            <MenuItem eventKey={7.2} href="#etf/finder">ETF Finder</MenuItem>
            <MenuItem eventKey={7.3} href="#etf/investmentcompanies">ETF Issuers</MenuItem>
          </NavDropdown>
          <NavDropdown eventKey={8} title="Bonds" id="main-menu-bonds">
            <MenuItem eventKey={8.1} href="#bonds">Bonds</MenuItem>
            <MenuItem eventKey={8.1} href="#bonds/libor">LIBOR</MenuItem>
            <MenuItem eventKey={8.2} href="#bonds/finder">Bonds Finder</MenuItem>
            <MenuItem eventKey={8.3} href="#bankrate/mortgage">Rates</MenuItem>
          </NavDropdown>
          <NavDropdown eventKey={9} title="Calendars" id="main-menu-calendars">
            <MenuItem eventKey={9.1} href="#calendars/economic">Economic</MenuItem>
            <MenuItem eventKey={9.1} href="#calendars/earnings">Earnings</MenuItem>
          </NavDropdown>
        </Nav>
      </Navbar.Collapse>
    )
  }
}

export default MainMenu