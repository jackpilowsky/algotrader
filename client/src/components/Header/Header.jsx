import React, { Component } from 'react';
import { Navbar, Row } from 'react-bootstrap';
import MainMenu from './MainMenu.jsx';
import UpperHeader from './UpperHeader.jsx';

class Header extends Component{
  constructor(props){
    super(props);
    this.state = {
      height: 0
    }
  }
  componentDidMount(){
    const height = document.getElementById('header-bar').clientHeight;
    this.setState({ height });
  }
  render(){
    const spacer = {
      marginTop: this.state.height
    }
    return (
      <header>
        <Navbar fixedTop id="header-bar">
          <UpperHeader />
          <Row className="header-nav-1">
            <div className="container">
              <Row className="hidden-xs">
                <MainMenu />
              </Row>
            </div>
          </Row>
        </Navbar>
        <div style={spacer}></div>
      </header>
    )
  }  
}

export default Header;