import React, { Component } from 'react';
import {AsyncTypeahead} from 'react-bootstrap-typeahead';
import Client from './../utils/Client'


class SearchArea extends Component{
  constructor(props){
    super(props);
    this.state = {
      minLength: props.minLength || 2,
      query: '',
      data: []
    }
  }  
  _handleChange = e => {
    if(!e || !e[0] || !e[0].symbol){
      return;
    }
    window.location.hash = '#stocks/' + e[0].symbol; 
  } 
  _handleSearch = query => {
    if (!query) {
      return;
    }
    if(query.length >= this.state.minLength){
      this.setState({query: query}, () => { 
        this._fetchData(); 
      });  
    }
  }
  _fetchData(){
    const self = this;
    Client.search('/api/stocks', this.state.query, (data) => {
      console.log('got results')
      const cleanedData = data.map((stock) => {
        return {
          id: stock.stockid,
          label: stock.symbol + ': ' + stock.companyname,
          symbol: stock.symbol
        }
      });
      self.setState({data: cleanedData},() => {
        this.render();
      });
    })
  }

  render(){
    return (
      <div className="search-area">
        <form name="mmssearch" action="/searchresults" method="GET" role="search" className="search-form">
          <div className="form-item">
            <i className="fa fa-search" />
            <AsyncTypeahead onChange={this._handleChange} onSearch={this._handleSearch} options={this.state.data}  />
          </div>
        </form>
      </div>
    )
  }
}
export default SearchArea
