import React, { Component } from 'react';
import {  Switch, Route, browserHistory } from 'react-router'
import Home from './../home/Home.jsx'


class Content extends Component{
  render(){
    return(
      <Switch>
        <Route exact path='/' component={Home}/>
        {/* both /roster and /roster/:number begin with /roster */}
        <Route path='/roster' component={Home}/>
        <Route path='/schedule' component={Home}/>
      </Switch>
    );
  }
}
export default Content