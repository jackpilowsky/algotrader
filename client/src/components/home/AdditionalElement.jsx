import React, { Component } from 'react';
import { Col } from 'react-bootstrap';

class AdditionalElement extends Component{
  render(){
    return(
      <Col sm={3}>
        <span>
            <a href={this.props.link}>{this.props.title}</a>
        </span>
        <span>
          <div data-field="Mid" data-item="Y0306000000XAU-USD" data-template="Mid" data-source="mdsng" data-table="1" data-room="RY0306000000XAU">
            <span className="push-data " data-format="minimumFractionDigits:2;maximumFractionDigits:2" data-animation="">1,276.33</span>
          </div>      
        </span>
        <div data-source="mdsng" data-table="1" data-item="Y0306000000XAU-USD" className="" data-room="RY0306000000XAU" data-field="changeper">
          <span className="push-data colorGreen " data-format="maximumFractionDigits:2" data-animation="animationType:color" data-jsvalue="1274.5000">0.14%</span>
        </div>    
      </Col>
    )
  }
}
export default AdditionalElement