import React, { Component } from 'react';
import MarketsNow from './MarketsNow.jsx'

class Home extends Component{
  constructor(props){
    super(props);
  }
  render(){
    return(
      <div className="container">
        <MarketsNow />
      </div>
    )  
  }
}
export default Home