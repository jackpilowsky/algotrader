import React, { Component } from 'react';
import { Col, Tabs, Tab } from 'react-bootstrap';
import MarketsElement from './MarketsElement.jsx';
import AdditionalElement from './AdditionalElement.jsx';

class MarketsNow extends Component{
  constructor(props){
    console.log(MarketsElement)
    console.log(AdditionalElement)
    super(props)
    this.state = {
      key:0
    }
  }
  componentWillMount(){
    this.setState({key: 1})
  }
  handleSelect(e){
    console.log(e)
    this.setState({key: e.target.value}) 
  }  
  render(){
    return(
      <div className="marketsnow">
        <Col sm={12} className="no-padding">
          <h1 className="box-headline markets-now-headline">Markets Now</h1>
        </Col>
        <Tabs activeKey={this.state.key} onSelect={this.handleSelect} id="markets-now-tabs">
          <Tab eventKey={1} title="Overview">
            <MarketsElement 
              title="DOW 30" 
              link="/index/dow_jones" 
              imgSrc="http://proxy.markets.businessinsider.com/cst/MarketsInsiderV2/Share/chart.aspx?instruments=300013,998313,310,333&amp;style=intraday_index_colortripleblau_early&amp;period=IntradayAvailability&amp;timezone=Eastern Standard Time&amp;la=1&amp;height=125&amp;width=150" />
            <MarketsElement 
              title="S&P 500" 
              link="/index/s&p_500" 
              imgSrc="http://proxy.markets.businessinsider.com/cst/MarketsInsiderV2/Share/chart.aspx?instruments=300013,998313,310,333&amp;style=intraday_index_colortripleblau_early&amp;period=IntradayAvailability&amp;timezone=Eastern Standard Time&amp;la=1&amp;height=125&amp;width=150" />
            <MarketsElement 
              title="NASDAQ 100" 
              link="/index/s&p_500" 
              imgSrc="http://proxy.markets.businessinsider.com/cst/MarketsInsiderV2/Share/chart.aspx?instruments=300013,998313,310,333&amp;style=intraday_index_colortripleblau_early&amp;period=IntradayAvailability&amp;timezone=Eastern Standard Time&amp;la=1&amp;height=125&amp;width=150" />
            <div className="marketsnow-additional-elements hidden-xs hidden-sm">
              <AdditionalElement title="gold" link="/commodities/gold-price" />
              <AdditionalElement title="Oil (WTI)" link="/commodities/oil-price?type=WTI" />
              <AdditionalElement title="EUR/USD" link="/currencies/EUR-USD" />
              <AdditionalElement title="BITCOIN" link="/currencies/BTC-USD" />
            </div>
          </Tab>
          <Tab eventKey={2} title="America">
            <MarketsElement 
              title="DOW 30" 
              link="/index/dow_jones" 
              imgSrc="http://proxy.markets.businessinsider.com/cst/MarketsInsiderV2/Share/chart.aspx?instruments=300013,998313,310,333&amp;style=intraday_index_colortripleblau_early&amp;period=IntradayAvailability&amp;timezone=Eastern Standard Time&amp;la=1&amp;height=125&amp;width=150" />
            <MarketsElement 
              title="S&P 500" 
              link="/index/s&p_500" 
              imgSrc="http://proxy.markets.businessinsider.com/cst/MarketsInsiderV2/Share/chart.aspx?instruments=300013,998313,310,333&amp;style=intraday_index_colortripleblau_early&amp;period=IntradayAvailability&amp;timezone=Eastern Standard Time&amp;la=1&amp;height=125&amp;width=150" />
            <MarketsElement 
              title="NASDAQ 100" 
              link="/index/s&p_500" 
              imgSrc="http://proxy.markets.businessinsider.com/cst/MarketsInsiderV2/Share/chart.aspx?instruments=300013,998313,310,333&amp;style=intraday_index_colortripleblau_early&amp;period=IntradayAvailability&amp;timezone=Eastern Standard Time&amp;la=1&amp;height=125&amp;width=150" />
            <div className="marketsnow-additional-elements hidden-xs hidden-sm">
              <AdditionalElement title="gold" link="/commodities/gold-price" />
              <AdditionalElement title="Oil (WTI)" link="/commodities/oil-price?type=WTI" />
              <AdditionalElement title="EUR/USD" link="/currencies/EUR-USD" />
              <AdditionalElement title="BITCOIN" link="/currencies/BTC-USD" />
            </div>
          </Tab>
          <Tab eventKey={3} title="Europe">
            <MarketsElement 
              title="DOW 30" 
              link="/index/dow_jones" 
              imgSrc="http://proxy.markets.businessinsider.com/cst/MarketsInsiderV2/Share/chart.aspx?instruments=300013,998313,310,333&amp;style=intraday_index_colortripleblau_early&amp;period=IntradayAvailability&amp;timezone=Eastern Standard Time&amp;la=1&amp;height=125&amp;width=150" />
            <MarketsElement 
              title="S&P 500" 
              link="/index/s&p_500" 
              imgSrc="http://proxy.markets.businessinsider.com/cst/MarketsInsiderV2/Share/chart.aspx?instruments=300013,998313,310,333&amp;style=intraday_index_colortripleblau_early&amp;period=IntradayAvailability&amp;timezone=Eastern Standard Time&amp;la=1&amp;height=125&amp;width=150" />
            <MarketsElement 
              title="NASDAQ 100" 
              link="/index/s&p_500" 
              imgSrc="http://proxy.markets.businessinsider.com/cst/MarketsInsiderV2/Share/chart.aspx?instruments=300013,998313,310,333&amp;style=intraday_index_colortripleblau_early&amp;period=IntradayAvailability&amp;timezone=Eastern Standard Time&amp;la=1&amp;height=125&amp;width=150" />
            <div className="marketsnow-additional-elements hidden-xs hidden-sm">
              <AdditionalElement title="gold" link="/commodities/gold-price" />
              <AdditionalElement title="Oil (WTI)" link="/commodities/oil-price?type=WTI" />
              <AdditionalElement title="EUR/USD" link="/currencies/EUR-USD" />
              <AdditionalElement title="BITCOIN" link="/currencies/BTC-USD" />
            </div>
          </Tab>
          <Tab eventKey={4} title="Asia">
            <MarketsElement 
              title="DOW 30" 
              link="/index/dow_jones" 
              imgSrc="http://proxy.markets.businessinsider.com/cst/MarketsInsiderV2/Share/chart.aspx?instruments=300013,998313,310,333&amp;style=intraday_index_colortripleblau_early&amp;period=IntradayAvailability&amp;timezone=Eastern Standard Time&amp;la=1&amp;height=125&amp;width=150" />
            <MarketsElement 
              title="S&P 500" 
              link="/index/s&p_500" 
              imgSrc="http://proxy.markets.businessinsider.com/cst/MarketsInsiderV2/Share/chart.aspx?instruments=300013,998313,310,333&amp;style=intraday_index_colortripleblau_early&amp;period=IntradayAvailability&amp;timezone=Eastern Standard Time&amp;la=1&amp;height=125&amp;width=150" />
            <MarketsElement 
              title="NASDAQ 100" 
              link="/index/s&p_500" 
              imgSrc="http://proxy.markets.businessinsider.com/cst/MarketsInsiderV2/Share/chart.aspx?instruments=300013,998313,310,333&amp;style=intraday_index_colortripleblau_early&amp;period=IntradayAvailability&amp;timezone=Eastern Standard Time&amp;la=1&amp;height=125&amp;width=150" />
            <div className="marketsnow-additional-elements hidden-xs hidden-sm">
              <AdditionalElement title="gold" link="/commodities/gold-price" />
              <AdditionalElement title="Oil (WTI)" link="/commodities/oil-price?type=WTI" />
              <AdditionalElement title="EUR/USD" link="/currencies/EUR-USD" />
              <AdditionalElement title="BITCOIN" link="/currencies/BTC-USD" />
            </div>
          </Tab>
          <Tab eventKey={5} title="Commodities">
            <MarketsElement 
              title="DOW 30" 
              link="/index/dow_jones" 
              imgSrc="http://proxy.markets.businessinsider.com/cst/MarketsInsiderV2/Share/chart.aspx?instruments=300013,998313,310,333&amp;style=intraday_index_colortripleblau_early&amp;period=IntradayAvailability&amp;timezone=Eastern Standard Time&amp;la=1&amp;height=125&amp;width=150" />
            <MarketsElement 
              title="S&P 500" 
              link="/index/s&p_500" 
              imgSrc="http://proxy.markets.businessinsider.com/cst/MarketsInsiderV2/Share/chart.aspx?instruments=300013,998313,310,333&amp;style=intraday_index_colortripleblau_early&amp;period=IntradayAvailability&amp;timezone=Eastern Standard Time&amp;la=1&amp;height=125&amp;width=150" />
            <MarketsElement 
              title="NASDAQ 100" 
              link="/index/s&p_500" 
              imgSrc="http://proxy.markets.businessinsider.com/cst/MarketsInsiderV2/Share/chart.aspx?instruments=300013,998313,310,333&amp;style=intraday_index_colortripleblau_early&amp;period=IntradayAvailability&amp;timezone=Eastern Standard Time&amp;la=1&amp;height=125&amp;width=150" />
            <div className="marketsnow-additional-elements hidden-xs hidden-sm">
              <AdditionalElement title="gold" link="/commodities/gold-price" />
              <AdditionalElement title="Oil (WTI)" link="/commodities/oil-price?type=WTI" />
              <AdditionalElement title="EUR/USD" link="/currencies/EUR-USD" />
              <AdditionalElement title="BITCOIN" link="/currencies/BTC-USD" />
            </div>
          </Tab>
          <Tab eventKey={6} title="Currencies">
            <MarketsElement 
              title="DOW 30" 
              link="/index/dow_jones" 
              imgSrc="http://proxy.markets.businessinsider.com/cst/MarketsInsiderV2/Share/chart.aspx?instruments=300013,998313,310,333&amp;style=intraday_index_colortripleblau_early&amp;period=IntradayAvailability&amp;timezone=Eastern Standard Time&amp;la=1&amp;height=125&amp;width=150" />
            <MarketsElement 
              title="S&P 500" 
              link="/index/s&p_500" 
              imgSrc="http://proxy.markets.businessinsider.com/cst/MarketsInsiderV2/Share/chart.aspx?instruments=300013,998313,310,333&amp;style=intraday_index_colortripleblau_early&amp;period=IntradayAvailability&amp;timezone=Eastern Standard Time&amp;la=1&amp;height=125&amp;width=150" />
            <MarketsElement 
              title="NASDAQ 100" 
              link="/index/s&p_500" 
              imgSrc="http://proxy.markets.businessinsider.com/cst/MarketsInsiderV2/Share/chart.aspx?instruments=300013,998313,310,333&amp;style=intraday_index_colortripleblau_early&amp;period=IntradayAvailability&amp;timezone=Eastern Standard Time&amp;la=1&amp;height=125&amp;width=150" />
            <div className="marketsnow-additional-elements hidden-xs hidden-sm">
              <AdditionalElement title="gold" link="/commodities/gold-price" />
              <AdditionalElement title="Oil (WTI)" link="/commodities/oil-price?type=WTI" />
              <AdditionalElement title="EUR/USD" link="/currencies/EUR-USD" />
              <AdditionalElement title="BITCOIN" link="/currencies/BTC-USD" />
            </div>
          </Tab>
        </Tabs>
      </div>  
    )
  }
}
export default MarketsNow