import React, { Component } from 'react';
import { Col } from 'react-bootstrap';

class MarketsElement extends Component{
  render(){
    return (
      <Col md={4} className="marketsnow-element">
        <Col xs={12} className=" no-padding-left no-padding-right">
          <Col xs={6} sm={4} md={6} className="no-padding-left no-padding-right">
            <div className="name">
              <a href={this.props.link}>{this.props.title}</a>
            </div>
            <div className="change-absolute">
              <span className="icon-set arrow-up"></span>
              <div data-source="mdsng" data-table="1" data-item="Y0306000000DJIA" data-room="RY0306000000DJIA" data-field="changeabs">
                <span className="push-data colorGreen " data-format="minimumFractionDigits:2;maximumFractionDigits:2" data-animation="animationType:color" data-jsvalue="23273.9600">167.80</span>
              </div>    
            </div>
            <Col xs={12} className="quote visible-xs visible-sm">
              <Col md={6} className="quote hidden-xs hidden-sm">
                <div data-field="Mid" data-item="Y0306000000DJIA" data-template="Mid" data-source="mdsng" data-table="1" data-room="RY0306000000DJIA">
                  <span className="push-data " data-format="minimumFractionDigits:2;maximumFractionDigits:2" data-animation="">23,441.76</span>
                </div>   
              </Col>
              <Col md={6} className="change-percent hidden-xs hidden-sm text-right">
                <div data-source="mdsng" data-table="1" data-item="Y0306000000DJIA" data-room="RY0306000000DJIA" data-field="changeper">
                  <span className="push-data colorGreen colorGreen " data-format="maximumFractionDigits:2" data-animation="animationType:color" data-jsvalue="23273.9600">0.72%</span>
                </div>    
              </Col>
              <span className="quotetime">
                <div data-field="MidTimestamp" data-item="Y0306000000DJIA" data-template="MidTimestamp" data-source="mdsng" data-table="1" className="" data-room="RY0306000000DJIA">
                <span className="push-data " data-format="utcToApplicationOffset:-4;useTwelveHourFormat:true" data-animation="animationType:none">04:20:01 PM</span>
                </div>
              </span>
            </Col>
            <Col xs={6} sm={8} md={6} className="no-padding-left no-padding-right">
              <a href={this.props.link}>
                <img src={this.props.imgSrc} alt="" className="markets-now-chart" />
              </a>
            </Col>
          </Col>
        </Col>
      </Col>
    )
  }
}

export default MarketsElement