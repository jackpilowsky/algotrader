import React, { Component } from 'react';
import Range from './../utils/Range';

class AZIndex extends Component{
  constructor(props){
    super(props);
    this.state = {
      letters: Range('A','Z') 
    }
  }  
  render(){
    return(
      <div className="further-stocks footer-box text-center warmGrey">
        <b>S&amp;P500 Stocks:</b>
        <a href="/stocks/indeces/S&amp;P_500">ALL</a>
        <a href="/stocks/indeces/S&amp;P_500/0-9">0-9</a>
        {
          this.state.letters.map((letter) => {
            return <a href="/stocks/indeces/S&amp;P_500/{letter}" key={letter}>{letter}</a>
          })
        }
      </div>
    )
  }
}
export default AZIndex