import React, { Component } from 'react';
import AZIndex from './AZIndex.jsx';

class Footer extends Component{
  render(){
    return(
      <div className="footer container">
        <hr />
        <div className="col-xs-12 no-padding">
          <div className="spacer-20"></div>

          <AZIndex />
          <div className="spacer-10"></div>
          
          <div className="further-stocks footer-box text-center warmGrey">
            <a href="/index/dow_jones">Dow Jones</a>
            <a href="/commodities/gold-price">Gold Price</a>
            <a href="/commodities/oil-price?type=WTI">Oil Price</a>
            <a href="/currencies/EUR-USD">EURO DOLLAR</a>
            <a href="/currencies/USD-CAD">CAD USD</a>
            <a href="/currencies/USD-MXN">PESO USD</a>
            <a href="/currencies/GBP-USD">POUND USD</a>
            <a href="/currencies/USD-INR">USD INR</a>
            <a href="/currencies/BTC-USD">Bitcoin Price</a>
            <a href="/currency-converter">Currency Converter</a>
            <a href="/currencies">Exchange Rates</a>
            <a href="/index/realtime-chart">Realtime Quotes</a>
            <a href="/premarket">Premarket</a>
            <a href="/stock/GOOG-Quote">Google Stock</a>
            <a href="/stock/AAPL-Quote">Apple Stock</a>
            <a href="/stock/FB-Quote">Facebook Stock</a>
            <a href="/stock/AMZN-Quote">Amazon Stock</a>
            <a href="/stock/TSLA-Quote">Tesla Stock</a>
          </div>
          <div className="spacer-10"></div>

          <div className="footer-box text-center warmGrey">
            * Copyright © 2017 Business Insider Inc. and <span className="skimlinks-unlinked">finanzen.net</span> GmbH <a href="http://www.finanzen.net/impressum">(Imprint)</a>. All rights reserved. Registration on or use of this site constitutes acceptance of our
            <a href="http://www.businessinsider.com/terms">Terms of Service</a> and <a href="http://www.businessinsider.com/privacy-policy">Privacy Policy</a>. <br/>

            <a href="http://www.businessinsider.com/disclaimer-finance">Disclaimer</a>
            |
            <a href="http://www.businessinsider.com/commerce-on-business-insider">Commerce Policy</a>
            |
            <a href="http://nytm.org/made">Made in NYC</a> | Stock quotes by <a href="http://www.finanzen.net">finanzen.net</a>
            <br/>
            <br/>
            <a href="https://my.markets.businessinsider.com/contact">Need help? Contact us!</a>
          </div>
        </div>
        <div id="mdsng_starter"></div>
      </div>
    )
  }  
}  

export default Footer