import React, { Component } from 'react';
import { Col  } from 'react-bootstrap';
import SearchArea from './SearchArea.jsx';

class UpperHeader extends Component{
  render(){
    return(
      <div id="upperheader">
        <nav className="header-logo hidden-xs">
          <div className="row header-logo-link">
            <Col sm={4} id="business-insider-area" >
              <a href="http://www.businessinsider.com/" target="_blank" rel="noopener noreferrer">
                <span className="icon-set btextCenterusiness-insider-logo">&nbsp;</span>
              </a>
              &nbsp;
            </Col>
            <Col sm={4} className="text-center mx-auto">
              <a href="#">
                <h1>Algo Trader</h1>
              </a>
            </Col>
            <Col sm={4} className="no-padding-left text-right" id="options-area">
              <a href="#mymarkets" className="btn btn-primary m-t m-b">
                <span>PORTFOLIO</span>
              </a>
              <SearchArea />
            </Col>
          </div>
        </nav>
      </div>
    )
  }  
}
export default UpperHeader