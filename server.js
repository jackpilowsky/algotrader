const express = require("express");
const fs = require("fs");
const session = require('express-session');
const FileStore = require('session-file-store')(session);
const bodyParser = require('body-parser')
const app = express();
const AccountRoutes = require('./lib/routes/AccountRoutes');
const StockRoutes = require('./lib/routes/StockRoutes');
const NasdaqMiner = require('./lib/data-miners/NasdaqMiner');
const nasdaqMiner = new NasdaqMiner();

app.use(session({
  name: 'algotrader',
  secret: 'mrnmokakmckmwa',
  saveUninitialized: true,
  resave: true,
  store: new FileStore()
}));

// set port
app.set("port", process.env.PORT || 3001);

// Express only serves static assets in production
if (process.env.NODE_ENV === "production") {
  app.use(express.static("client/build"));
}
// set routes
app.use(AccountRoutes);
app.use(StockRoutes);

// data miners
nasdaqMiner.mine();

// log initilization
app.listen(app.get("port"), () => {
  console.log(`Find the server at: http://localhost:${app.get("port")}/`); // eslint-disable-line no-console
});
