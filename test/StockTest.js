const mocha = require('mocha');
const chai = require('chai');
const expect = chai.expect; // we are using the "expect" style of Chai
const StockController = require('./../controllers/StockController');
const StockModel = require('./../models/StockModel');

class StockTest{
  runTests(){
    console.log('running tests')
    const stockController = new StockController();
    const stockModel = new StockModel();
    const stock = {
      symbol: 'MSFT',
      companyname: 'Microsoft'
    }
    stockModel.emptyTable();
    // stockController.get
    describe('stockController.get', function() {
      it('stockController.get() should return an empty array', function() {
        expect(stockController.get()).to.be.a('array').that.is.empty;
      });
    });
    // stockModel create
    describe('stockModel.create', function() {
      it('stockModel.create() should return an array of 1', function() {
        expect(stockModel.create(stock)).to.be.a('array').that.have.lengthOf(1);
      });
    });

    // stockModel list
    describe('stockModel.list', function() {
      it('stockModel.list() should return an array of 1 and that have our stock', function() {
        expect(stockModel.list()).to.be.a('array').that.have.lengthOf(1).that.have.deep.members([stock]);;
      });
    });

    // stockModel get
    describe('stockModel.get', function() {
      it('stockModel.get() should get the stock', function() {
        let stocks = stockModel.list();
        // there should only be 1 stock in the database at this point
        let stockid = stocks[0].stockid;
        expect(stockModel.get({stockid: stockid})).to.be.a('array').that.have.lengthOf(1).that.have.deep.members([stock]);
      });
    });

    // stockModel update
    describe('stockModel.update', function() {
      it('stockModel.update() should update the stock', function() {
        // there should only be 1 stock in the database at this point
        let stocks = stockModel.list();
        let updatedStock = {
          symbol: 'MSFT2',
          companyname: 'Microsoft Corp',
          stockid: stocks[0].stockid
        }
        stockModel.update(updatedStock);
        expect(stockModel.get({stockid: stockid})).to.be.a('array').that.have.lengthOf(1).that.have.deep.members([updatedStock]);;
      });
    });

    // stockModel delete
    describe('stockModel.delete', function() {
      it('stockModel.delete() should update the stock', function() {
        // there should only be 1 stock in the database at this point
        let stocks = stockModel.list();
        let stockid = stocks[0].stockid;
        stockModel.delete({stockid: stockid});
        expect(stockController.get()).to.be.a('array').that.is.empty;
      });
    });
  }
}

module.exports = StockTest;